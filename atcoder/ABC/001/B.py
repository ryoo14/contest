m = int(input())

if m < 100:
    ans = 0
elif 100 <= m <= 5000:
    ans = int(m / 100)
elif 6000 <= m <= 30000:
    ans = int(m / 1000 + 50)
elif 35000 <= m <= 70000:
    ans = int((m / 1000 - 30) / 5 + 80)
else:
    ans = 89

if ans < 10:
    ans = '0' + str(ans)

print(ans)
