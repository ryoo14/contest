s = gets.chomp!.split('')
t = gets.chomp!.split('')

flag = true
s.each_with_index do |v,i|
  if v != t[i] then
    if v == '@' then
      if t[i] !~ /[atcoder]/ then
        flag = false
        break
      end
    elsif t[i] == '@' then
      if v !~ /[atcoder]/ then
        flag = false
        break
      end
    else
      flag = false
      break
    end
  end  
end

if flag then
  puts "You can win"
else
  puts "You will lose"
end
