s = int(input())
li = []
i = 1

while True:
    li.append(s)

    if s % 2 == 0:
        s = s / 2
    elif s % 2 != 0:
        s = 3 * s + 1

    i += 1

    if s in li:
        break

print(i)

