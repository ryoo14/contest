n = int(input())
l = list(map(int, input().split()))

m = max(l)
nn = sum(l) - m
if nn > m:
    print('Yes')
else:
    print('No')
