import numpy as np
import heapq

n, m = map(int, input().split())
x = list(map(int, input().split()))
x.sort()

if n >= m:
    print(0)
else:
    npx = np.array(x)
    li = list(np.diff(npx, n = 1))

    print(sum(heapq.nsmallest(len(li)-n+1, li)))
