import numpy as np
n, m = map(int, input().split())
a = np.array(list(map(int, input().split())))

mx = 0
mxi = 0
for i in range(m):
    l = i + 1
    if mx < sum(a^l):
        mx = sum(a^l)

print(mx)

