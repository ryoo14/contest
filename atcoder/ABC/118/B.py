n,m = map(int, input().split())

li = []
for i in range(n):
    lis = list(map(int, input().split()))
    lis.pop(0)
    li += lis

ans = 0
for i in range(m):
    if n == li.count(i+1):
        ans += 1

print(ans)
