n = int(input())
a = list(map(int, input().split()))

mi = min(a)
ma = max(a)
if mi == ma:
    print(mi)
else:
    su = sum(a) % mi
    if su == 0:
        print(mi)
    else:
        print(su)
