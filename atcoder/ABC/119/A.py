import datetime

heisei = datetime.date(2019, 4, 30)
y, m, d = map(int, input().split('/'))
s = datetime.date(y, m, d)

if s > heisei:
    print('TBD')
else:
    print('Heisei')
