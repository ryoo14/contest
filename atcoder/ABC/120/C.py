k = input()
z = k.count('0')
o = k.count('1')

if len(k) == 1:
    print(0)
else:
    if z == 0 or o == 0:
        print (0)
    else:
        if z < o:
            print(z*2)
        else:
            print(o*2)
