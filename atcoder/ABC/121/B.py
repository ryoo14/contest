n,m,c = map(int,input().split())
b = list(map(int,input().split()))

ans = 0
for i in range(n):
    d = list(map(int,input().split()))
    su = 0
    for l in range(m):
        su += b[l]*d[l]
    if su+c > 0:
        ans += 1

print(ans)
