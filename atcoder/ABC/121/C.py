n,m = map(int,input().split())
b = []
for i in range(n):
    b.append(list(map(int,input().split())))

b.sort()

su = 0
for i in range(len(b)):
    if b[i][1] >= m:
        su += b[i][0]*m
        m = 0
    else:
        su += b[i][0]*b[i][1]
        m -= b[i][1]

    if m == 0:
        break

print(su)
