from functools import reduce

a,b=map(int,input().split())
print(reduce(lambda c,d: c^d, range(a,b+1)))
