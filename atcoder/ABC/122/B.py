S = input()

ans = 0
cnt = 0
for i in S:
    if i in "ACGT":
        cnt += 1
    else:
        cnt = 0

    if ans < cnt:
        ans = cnt

print(ans)
