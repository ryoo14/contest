using System;
using System.Collections.Generic;
using System.Linq;

public class Hello
{
    private const int MOD_N = 1000000000 + 7;

    public static void Main()
    {
        var list = Console.ReadLine().Split(' ');
        var n = int.Parse(list[0]);
        var m = int.Parse(list[1]);

        var list2 = Console.ReadLine().Split(' ');
        var ass = new uint[n];
        for (var i = 0; i < ass.Length; ++i)
            ass[i] = uint.Parse(list2[i]);

        var bcs = new KeyValuePair<int, uint>[m];
        for (var i = 0; i < bcs.Length; ++i)
        {
            var list3 = Console.ReadLine().Split(' ');
            var b = int.Parse(list3[0]);
            var c = uint.Parse(list3[1]);
            bcs[i] = new KeyValuePair<int, uint>(b, c);
        }

        Array.Sort(ass);
        Array.Sort(bcs, (l, r) => r.Value.CompareTo(l.Value));

        for (int i = 0, j = 0; i < bcs.Length; ++i)
        {
            var bc = bcs[i];
            for (int k = 0; k < bc.Key; ++k)
            {
                if (bc.Value > ass[j])
                {
                    ass[j] = bc.Value;
                    ++j;
                    if (j >= ass.Length)
                        goto Finish;
                }
                else
                    goto Finish;
            }
        }

        Finish:
        ulong sum = 0;
        for (int i = 0; i < ass.Length; ++i)
            sum += ass[i];

        Console.WriteLine(sum);
    }
}
