n,m = gets.split.map(&:to_i)
a = []
flag = Array.new(n+1,true)
MD = 10**9+7

m.times do
  a = gets.to_i
  flag[a] = false
end

step = []
step[0] = 0
step[1] = flag[1] ? 1 : 0
if n == 1
  puts step[1]
  exit
end
step[2] = flag[2] ? step[1]+1 : 0
(3..n).each do |i|
  if flag[i]
    step[i] = (step[i-1]+step[i-2])%MD
  else
    step[i] = 0
  end
end

puts step[-1]
