using System;
using System.Collections.Generic;
using System.Linq;

public class Hello
{
    private const int MOD_N = 1000000000 + 7;

    public static void Main()
    {
        var list = Console.ReadLine().Split(' ');
        var n = int.Parse(list[0]);
        var m = int.Parse(list[1]);
        var ass = new int[m];
        for (int i = 0; i < ass.Length; ++i)
            ass[i] = int.Parse(Console.ReadLine());

        int pp = 1;
        int prev = 0;
        int current = 0;

        int ai = 0;
        var a = ai < ass.Length ? ass[ai] : -1;

        if (a == 1)
        {
            prev = 0;
            ++ai;
            if (ai < ass.Length)
                a = ass[ai];
        }
        else
        {
            prev = pp;
        }

        for (int i = 2; i <= n; ++i)
        {
            if (i == a)
            {
                current = 0;
                ++ai;
                if (ai < ass.Length)
                    a = ass[ai];
            }
            else
            {
                current = prev + pp;
                current %= MOD_N;
            }
            pp = prev;
            prev = current;
        }

        Console.WriteLine(prev);
    }

    private static int Comb(int m, int n)
    {
        var t = m - n;
        if (n > t)
            n = t;

        long numerator = 1;
        for (int i = 0; i < n; ++i)
        {
            numerator *= m - i;
            numerator %= MOD_N;
        }
        long denominator = 1;
        for (; n != 0; --n)
        {
            denominator *= n;
            denominator %= MOD_N;
        }

        var inv_denominator = ModInv((int)denominator, MOD_N);
        var ret = (int)(numerator * inv_denominator % MOD_N);

        return ret >= 0 ? ret : ret + MOD_N;
    }

    private static int ModInv(int a, int m)
    {
        int b = m, x = 1, y = 0;
        while (b != 0)
        {
            var t = a / b;
            a -= t * b;
            Swap(ref a, ref b);
            x -= t * y;
            Swap(ref x, ref y);
        }
        x %= m;
        return x >= 0 ? x : x + m;
    }

    private static void Swap<T>(ref T a, ref T b)
    {
        var t = a;
        a = b;
        b = t;
    }
}
