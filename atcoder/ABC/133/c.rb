l,r = gets.split.map(&:to_i)

if l*r < 2019
  puts l*(l+1)
else
  if l <= 2019 and 2019 <= r
    puts 0
  else
    # 2019以下
    if r < 2019
      ama = 2019 / l
      if l <= ama and ama < r
        puts l*(ama+1)%2019
      else
        puts l*(l+1)%2019
      end
    # 2019以上
    else
      a = l / 2019
      am = l % 2019
      b = r / 2019
      bm = r % 2019
      if am == 0 or bm == 0
        puts 0
      elsif a != b
        puts 0
      else
        puts l*(l+1)%2019
      end
    end
  end
end
