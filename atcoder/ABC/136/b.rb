n = gets.chomp!

s = n.size
n = n.to_i

if s == 1
  puts n
elsif s == 2
  puts 9
elsif s == 3
  puts 9 + (n - 99)
elsif s == 4
  puts 909
elsif s == 5
  puts 9 + 900 + (n - 9999)
elsif s == 6
  puts 90909
end
