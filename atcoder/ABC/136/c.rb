n = gets.to_i
h = gets.split.map(&:to_i)
flag = true
hm = -1

if n == 1
  puts 'Yes'
else
  (n-1).times do |i|
    if h[i] < hm
      flag = false
      break
    end

    if h[i] > h[i+1]
      if h[i] - h[i+1] > 1
        flag = false
        break
      else
        if hm < h[i] - 1
          hm = h[i] - 1
        end
      end
    else
      if hm < h[i] - 1
        hm = h[i] - 1
      end
    end
  end

  if hm > h[-1]
    flag = false
  end


  if flag
    puts 'Yes'
  else
    puts 'No'
  end
end

