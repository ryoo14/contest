n = gets.to_i
b = gets.split.map(&:to_i)
a = [b[0]]

if n == 2
  puts b[0] * 2
else
  (b.size-1).times do |i|
    if b[i] > b[i+1]
      a << b[i+1]
    else
      a << b[i]
    end
  end
  if a[-1] > b[-1]
    a << a[-1]
  else
    a << b[-1]
  end

  puts a.inject(&:+)
end

