n = gets.to_i
d = Math.sqrt(n).to_i
ans = 0

1.upto(d) do |i|
  if n % i == 0
    ans = i
  end
end

if ans == 1
  puts n-1
else
  puts (n/ans)+ans-2
end
