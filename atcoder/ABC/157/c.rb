n,m = gets.split(' ').map(&:to_i)
l = []
m.times do
  l << gets.split(' ').map(&:to_i)
end

ans = Array.new(n)

if l.include?([1, 0])
  if n == 1
    puts 0
  else
    puts -1
  end
else
  f = true
  l.uniq.each do |v|
    if ans[v[0]-1] != nil
      f = false
      break
    end

    ans[v[0]-1] = v[1]
  end
  
  ans.each_with_index do |v,i|
    if v.nil?
      if i == 0 and n == 1
        ans[i] = 0
      elsif i == 0 and n != 0
        ans[i] = 1
      else
        ans[i] = 0
      end
    end
  end

  if f
    puts ans.join('')
  else
    puts -1
  end
end

