a, b, w = gets.split().map(&:to_i)

mi = 1000000000
ma = 0

(1..1000000).each do |i|
  if i*a <= 1000*w && 1000*w <= i*b
    mi = [mi, i].min
    ma = [ma, i].max
  end
end

if ma == 0
  puts 'UNSATISFIABLE'
else
  puts "#{mi} #{ma}"
end
