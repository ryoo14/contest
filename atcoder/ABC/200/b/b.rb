n, k = gets.split(' ').map(&:to_i)

k.times do
  if n % 200 == 0
    n /= 200
  else
    ns = n.to_s + "200"
    n = ns.to_i
  end
end

puts n
