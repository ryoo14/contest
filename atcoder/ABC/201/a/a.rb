a, b, c = gets.split(' ').map(&:to_i).sort
if (c - b) == (b - a)
  puts "Yes"
else
  puts "No"
end
