a, b, c = gets.split(' ').map(&:to_i)
if a != b and b != c and c != a
  puts 0
else
  if a == b
    puts c
  elsif b == c
    puts a
  elsif c == a
    puts b
  end
end
