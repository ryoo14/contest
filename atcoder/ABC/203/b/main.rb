n, k = gets.split(' ').map(&:to_i)
ans = 0

n.times do |i|
  k.times do |l|
    ans += ((i+1).to_s + '0' + (l+1).to_s).to_i
  end
end

puts ans
