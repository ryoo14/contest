n = gets.to_i
a = gets.split(' ').map(&:to_i)
if (1..n).to_a == a.sort
  puts 'Yes'
else
  puts 'No'
end
