a, b, c, d = gets.split(' ').map(&:to_i)

ans = 10 ** 5 + 1

if c*d-b < 0
  puts -1
else
  (1..a).each do |aa|
    if a <= (c*d - b) * aa
      ans = aa
      break
    end
  end
  if ans == 10 ** 5 +1
    puts -1
  else
    puts ans
  end
end
