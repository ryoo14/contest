p = gets.to_i

l = [1, 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800]

ans = 0
l.reverse_each do |i|
  if i <= p
    ans += p / i
    p = p % i
  else
    next
  end
end

puts ans

