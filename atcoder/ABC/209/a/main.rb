a, b = gets.split(' ').map(&:to_i)
if b < a
  puts 0
elsif a == b
  puts 1
else
  puts b - a + 1
end
