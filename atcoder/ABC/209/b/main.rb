n, x = gets.split(' ').map(&:to_i)
a = gets.split(' ').map(&:to_i)
sum = a.map.with_index { |aa, i| i % 2 == 0 ? aa : aa-1 }.sum
if sum <= x
  puts 'Yes'
else
  puts 'No'
end
