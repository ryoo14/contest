a = 0
b = 0

a1, b1 = gets.split(' ').map(&:to_i)
a2, b2 = gets.split(' ').map(&:to_i)
a3, b3 = gets.split(' ').map(&:to_i)

if a1 == a2
  a = a3
elsif a1 == a3
  a = a2
else
  a = a1
end

if b1 == b2
  b = b3
elsif b1 == b3
  b = b2
else
  b = b1
end

puts "#{a} #{b}"
