a, b, k = gets.split(' ').map(&:to_i)

if b <= a
  puts 0
else
  cnt = 0
  loop do
    cnt += 1
    a *= k
    if b <= a
      break
    end
  end

  puts cnt
end
