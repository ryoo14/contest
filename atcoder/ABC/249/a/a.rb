a, b, c, d, e, f, x = gets.split(' ').map(&:to_i)

tcount = x / (a+c)
acount = x / (d+f)

trest = x % (a+c)
arest = x % (d+f)

tmile = tcount * a * b + [trest, a].min * b
amile = acount * d * e + [arest, d].min * e


if amile < tmile
  puts "Takahashi"
elsif tmile < amile
  puts "Aoki"
else
  puts "Draw"
end
