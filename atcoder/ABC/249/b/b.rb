s = gets.chomp

flag = false
if s =~ /[A-Z]/
  if s =~ /[a-z]/
    if s.size == s.split('').uniq.size
      flag = true
    end
  end
end

if flag
  puts "Yes"
else
  puts "No"
end
