N = int(input())
S = input()
M = 10**9+7

cnt = {}
for i in S:
    if i in cnt.keys():
        cnt[i] += 1
    else:
        cnt[i] = 1

ans = 1
for l in cnt.values():
    ans *= l + 1

print((ans-1)%M)


