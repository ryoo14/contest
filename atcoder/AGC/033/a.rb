def to_black(xi,yi,h,w)
  l = []
  # up
  if xi != 0
    l << [xi-1,yi]
  end
  # down
  if xi != h-1
    l << [xi+1,yi]
  end
  # left
  if yi != 0
    l << [xi,yi-1]
  end
  # right
  if yi != w-1
    l << [xi,yi+1]
  end
  l
end

h,w = gets.split(' ').map(&:to_i)

li = Array.new
white_list = Array.new
h.times do |i|
  str = gets.chomp
  w.times do |j|
    if str[j] == '#'
      white_list += to_black(i,j,h,w)
    end
  end
  li << str.split('')
end

cnt = 0

while true
  if white_list.size == 0
    break
  end

  tmp = []
  c = true
  white_list.uniq!
  white_list.each do |v|
    if li[v[0]][v[1]] == '#'
      next
    end
    tmp += to_black(v[0],v[1],h,w)
    li[v[0]][v[1]] = '#'
    c = false
  end
  white_list = tmp

  if c
    break
  end
  cnt += 1
end

puts cnt
