h,w = gets.split.map(&:to_i)
INFI = 10000000

black = []
dmap = Array.new(h) { Array.new(w,INFI) }

h.times do |hi|
  gets.chomp.each_char.with_index do |c,wi|
    if c == '#'
      black.unshift([hi,wi])
      dmap[hi][wi] = 0
    end
  end
end

step = 0
while not black.empty?
  #hb,wb = black.shift
  n = []
  step += 1

  black.each do |hb,wb|
    if hb > 0
      if dmap[hb-1][wb] == INFI
        dmap[hb-1][wb] = step
        n << [hb-1,wb]
      end
    end

    if hb < h - 1
      if dmap[hb+1][wb] == INFI
        dmap[hb+1][wb] = step
        n << [hb+1,wb]
      end
    end

    if wb > 0
      if dmap[hb][wb-1] == INFI
        dmap[hb][wb-1] = step
        n << [hb,wb-1]
      end
    end

    if wb < w -1
      if dmap[hb][wb+1] == INFI
        dmap[hb][wb+1] = step
        n << [hb,wb+1]
      end
    end
  end

  black = n

end

puts step-1
