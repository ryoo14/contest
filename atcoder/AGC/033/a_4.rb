h,w = gets.split.map(&:to_i)

if h == 1 and w == 1
  puts 0
else
  dmap = Hash.new{ |s,k| s[k] = [10000000]*w }
  black = []
  h.times do |hi|
    str = gets.chomp
    w.times do |wi|
      if str[wi] == '#'
        black.push([hi, wi])
      end
    end
  end

  step = 0

  while true
    if black.size == 0
      break
    end
    step += 1
    n = []

    black.each do |hb,wb|
      if step < dmap[hb][wb]
        dmap[hb][wb] = step
        if hb > 0
          if step < dmap[hb-1][wb]
            n << [hb-1,wb]
          end
        end
        if hb < h -1
          if step < dmap[hb+1][wb]
            n << [hb+1,wb]
          end
        end
        if wb > 0
          if step < dmap[hb][wb-1]
            n << [hb,wb-1]
          end
        end
        if wb < w -1
          if step < dmap[hb][wb+1]
            n << [hb,wb+1]
          end
        end
      end
    end
    black = n
  end

  puts step-1
end
