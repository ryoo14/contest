
using System;
using System.Collections.Generic;
using System.Linq;

public class Hello
{
    public static void Main()
    {
        var line1 = Console.ReadLine().Split(' ');
        var h = int.Parse(line1[0]);
        var w = int.Parse(line1[1]);

        if (h == 1 && w == 1)
        {
            Console.WriteLine(0);
            return;
        }

        var map = new int[h, w];
        for (int i = 0; i < h; ++i)
            for (int j = 0; j < w; ++j)
                map[i, j] = int.MaxValue;

        var black = new List<KeyValuePair<int, int>>();

        for (int i = 0; i < h; ++i)
        {
            var line = Console.ReadLine();
            for (int j = 0; j < w; ++j)
            {
                if (line[j] == '#')
                    black.Add(new KeyValuePair<int, int>(i, j));
            }
        }

        var next = new List<KeyValuePair<int, int>>();

        int step = -1;

        while (black.Count > 0)
        {
            ++step;
            next.Clear();

            foreach (var p in black)
            {
                var i = p.Key;
                var j = p.Value;

                if (step < map[i, j])
                {
                    map[i, j] = step;
                    if (i > 0)
                        next.Add(new KeyValuePair<int, int>(i - 1, j));
                    if (i < h - 1)
                        next.Add(new KeyValuePair<int, int>(i + 1, j));
                    if (j > 0)
                        next.Add(new KeyValuePair<int, int>(i, j - 1));
                    if (j < w - 1)
                        next.Add(new KeyValuePair<int, int>(i, j + 1));
                }
            }

            var tmp = black;
            black = next;
            next = tmp;
        }

        Console.WriteLine(step - 1);
    }
}
