ss = gets.chomp
cnt = 0
ss = ss.gsub(/BC/,'?')

while ss.count('?') > 0
  i = ss.index('?')
  ii = ss.index(/A+\?/)
  if ii.nil?
    ss.gsub!(/\?/,'BC')
    break
  elsif i > ii
    cnt += i - ii
    iii = i - ii
    t = '?' * iii + 'A'
    ss[ii..i] = t
  elsif i < ii
    ss[i] = 'BC'
  end
end

puts cnt
