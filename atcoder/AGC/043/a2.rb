h, w = gets.split.map(&:to_i)
l = []
h.times do
  ll = gets.chomp.split('')
  l << ll
end

dp = Array.new(h,0){Array.new(w,0)}
h.times do |hi|
  w.times do |wi|
    if hi == 0
      if wi == 0
        if l[hi][wi] == '#'
          dp[hi][wi] += 1
        end
      else
        if l[hi][wi] == '#'
          if l[hi][wi-1] != '#'
            dp[hi][wi] += 1
          end
        end
        dp[hi][wi] += dp[hi][wi-1]
      end
    else
      if wi == 0
        if l[hi][wi] == '#'
          if l[hi-1][wi] != '#'
            dp[hi][wi] += 1
          end
        end
        dp[hi][wi] += dp[hi-1][wi]
      else
        if dp[hi-1][wi]==dp[hi][wi-1]
          if l[hi-1][wi]=='.' and l[hi][wi-1]=='#'
            if l[hi][wi] == '#'
              if l[hi][wi-1] != '#'
                dp[hi][wi] += 1
              end
            end
            dp[hi][wi] += dp[hi][wi-1]
          elsif l[hi][wi-1]=='.' and l[hi-1][wi]=='#'
            if l[hi][wi] == '#'
              if l[hi-1][wi] != '#'
                dp[hi][wi] += 1
              end
            end
            dp[hi][wi] += dp[hi-1][wi]
          else
            if l[hi][wi] == '#'
              if l[hi-1][wi] != '#'
                dp[hi][wi] += 1
              end
            end
            dp[hi][wi] += dp[hi-1][wi]
          end
        elsif dp[hi-1][wi]<dp[hi][wi-1]
          if l[hi][wi] == '#'
            if l[hi-1][wi] != '#'
              dp[hi][wi] += 1
            end
          end
          dp[hi][wi] += dp[hi-1][wi]
        else
          if l[hi][wi] == '#'
            if l[hi][wi-1] != '#'
              dp[hi][wi] += 1
            end
          end
          dp[hi][wi] += dp[hi][wi-1]
        end
      end
    end
  end
end
puts dp[h-1][w-1]
