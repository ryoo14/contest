n = gets.to_i
l = gets.split('').map(&:to_i)

while true
  ll = []
  l.each_cons(2) do |a,b|
    ll << (a-b).abs
  end

  l = ll
  if l.size == 1
    break
  end
end

puts ll[0]
