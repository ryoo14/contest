n = gets.to_i
l = gets.chomp.split('').map(&:to_i)

if n == 2
  puts l[0]-l[1]
else
  puts ((l[0]-l[1]).abs - (l[-1]-l[-2]).abs).abs
end
