r,g,b,n = gets.split.map(&:to_i)
result = 0

(n+1).times do |rn|
  (n+1).times do |gn|
    k = n - rn*r - gn*g
    if k % b == 0 and k >= 0
      result += 1
    end
  end
end

puts result
