n = gets.to_i
a = Array.new
b = Array.new
ab = Array.new
ans = 0

n.times do
  s = gets.chomp.gsub(/AB/, '!')
  ans += s.count('!')
  if s[0] == 'B' and s[-1] == 'A'
    ab << s
  elsif s[0] == 'B'
    b << s
  elsif s[-1] == 'A'
    a << s
  end
end

as = a.size
bs = b.size
abs = ab.size

if as == 0 and bs == 0
  if abs == 0
    puts ans
  else
    ans += abs - 1
    puts ans
  end
elsif as == 0 or bs == 0
  if abs == 0
    puts ans
  else
    if as == 0
      if abs <= bs
        ans += abs
      else
        ans += abs
      end
    else
      if abs <= as
        ans += abs
      else
        ans += abs
      end
    end
    puts ans
  end
else
  if abs == 0
    if as <= bs
      ans += as
    else
      ans += bs
    end
  else
    if as == bs
      if as == abs
        ans += as * 2
      else
        ans += as
        ans += abs
      end
    elsif as < bs
      ans += as
      t = bs - as
      if t == abs
        ans += t
      elsif t < abs
        ans += abs
      else
        ans += abs
      end
    elsif as > bs
      ans += bs
      t = as - bs
      if t == abs
        ans += t
      elsif t < abs
        ans += abs
      else
        ans += abs
      end
    end
  end
  puts ans
end
