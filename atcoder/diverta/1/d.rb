require 'complex'
n = gets.to_i
m = Math.sqrt(n).to_i
cnt = 0

(1..m).each do |i|
  if n % i == 0 and n / i - 1 > i
    cnt += n / i - 1
  end
end

puts cnt
