n = gets.to_i

li = []
sa = {}
n.times do
  li << gets.split.map(&:to_i)
end

if n == 1
  puts 1
else
  li.each_with_index do |v, i|
    if i+1 == n
      break
    end
    (i+1..n-1).each do |l|
      a = v[0]-li[l][0]
      b = v[1]-li[l][1]
      s = [a,b].join(' ')
      if sa.has_key?(s)
        sa[s] += 1
      else
        sa[s] = 1
      end
      s = [-a,-b].join(' ')
      if sa.has_key?(s)
        sa[s] += 1
      else
        sa[s] = 1
      end
    end
  end

  sb = sa.max {|a,b| a[1] <=> b[1]}

  puts n - sb[1]
end
