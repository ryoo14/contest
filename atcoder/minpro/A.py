n,k = map(int, input().split())

if n < k:
    print('NO')
else:
    g = n // 2
    kk = n - g

    if k <= g or k <= kk:
        print('YES')
    else:
        print('NO')
