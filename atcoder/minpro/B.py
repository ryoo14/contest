li = []
for i in range(3):
    a,b = map(int, input().split())
    li.append(a)
    li.append(b)

dic = {}

for i in li:
    if i in dic.keys():
        dic[i] += 1
    else:
        dic[i] = 1

dicc = {}
for i in dic.values():
    if i in dicc.keys():
        dicc[i] += 1
    else:
        dicc[i] = 1

if dicc[1] == 2 and dicc[2] == 2:
    print('YES')
else:
    print('NO')

