k,a,b = map(int, input().split())

if b - a < 2:
    print(1+k)
else:
    c = a - 1 + 2
    d = k - c
    if d % 2 == 0:
        print((d//2)*(b-a)+b)
    else:
        print((d//2)*(b-a)+b+1)
