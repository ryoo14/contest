s = gets.chomp.split('')
cnt = 0
sumo = 0
s.each do |i|
  sumo += 1
  if i == 'o'
    cnt += 1
  end
end

if cnt > 7
  puts 'YES'
else
  k = 15 - sumo
  if k+cnt > 7
    puts 'YES'
  else
    puts 'NO'
  end
end
