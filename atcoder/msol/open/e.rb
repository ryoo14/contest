Q = gets.to_i
AM = 1000003

Q.times do
  x,d,n = gets.split.map(&:to_i)
  ans = d**n % AM
  ans *= Math.gamma(x/d+n) % AM
  ans /= Math.gamma(x/d) % AM
  puts ans
end


