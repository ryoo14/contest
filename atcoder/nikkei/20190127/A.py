n, a, b = map(int, input().split())

m = 0
x = 0

if n >= a+b:
    x = min(a,b)
    m = 0
elif n < a+b:
    x = min(a,b)
    m = (a+b)-n

print(x,m)
